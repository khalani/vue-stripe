import Axios from "axios";

const options = {
  baseURL: process.env.VUE_APP_ROOT_API,
};
const axios = Axios.create(options);
export default class HttpService {
  constructor() {
    this.http = axios;
  }

  get(url) {
    return this.http.get(url);
  }

  post(url, data) {
    return this.http.post(url, data);
  }
}
