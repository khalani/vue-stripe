import { createStore } from "vuex";

import HttpService from "../helper/CommonHttp";

const HttpServiceInstance = new HttpService();

export default createStore({
  state: {
    products: [],
    payment: {},
  },
  getters: {
    getProducts(state) {
      return state.products;
    },
    getPayment(state) {
      return state.payment;
    },
  },
  mutations: {
    change(state, products) {
      state.products = products;
    },
    payment(state, payment) {
      state.payment = payment;
    },
  },
  actions: {
    setProducts({ commit }) {
      HttpServiceInstance.get("product/list").then((res) => {
        this.data = res.data;
        commit("change", this.data);
      });
    },
    payment({ commit }, paymentDetails) {
      HttpServiceInstance.post("product/payment", paymentDetails).then(
        (res) => {
          commit("payment", res.data);
        }
      );
    },
  },
  modules: {},
});
