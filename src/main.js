import { createApp } from "vue";
import DKToast from "vue-dk-toast";

import App from "./App.vue";
import router from "./router";
import store from './store'

createApp(App).use(store)
  .use(router)
  .use(DKToast, {
    duration: 5000,
    positionY: "bottom",
    positionX: "right",
    styles: {
      color: "#000",
      backgroundColor: "green",
      borderRadius: "25px",
    },
    slot: '<i class="fa fa-thumbs-up"></i>',
  })
  .mount("#app");
